const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')

module.exports = {
    stats: 'errors-only',
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, 'build'),
        filename: '[name].js'
    },
    devServer: {
        contentBase: path.join(__dirname, 'dist'),
        compress: true,
        port: 9000
    },
    plugins: [
        new CleanWebpackPlugin("build"),
        new HtmlWebpackPlugin({
            template: "./src/index.html",
            title: 'My App',
            hash: true
        })
    ],
    devtool: 'inline-source-map',
    module: {
        rules: [
            {test: /\.js$/, exclude: /node_modules/, use: 'babel-loader'},
            {test: /\.scss$/, loaders: ['style-loader', 'css-loader', 'sass-loader']}
        ]
    }
}