import React from "react"
import "./Tabs.scss"
import PropTypes from 'prop-types';

/**
 * Tabs component
 * @prop: align, values: left center right - align of tab buttons
 * @prop: name  required, type: array of strings  - tab buttons names
 * @children: component: Tab, required  - tab contents
 **/


export default class Tabs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {tab: 0}
    }

    render() {
        if (this.props.names)
            return <React.Fragment>
                <ul className="tabs" style={{textAlign: this.props.align || "center"}}>
                    {
                        this.props.names.map((el, index) =>
                            <li onClick={() => this.setState({tab: index})}
                                key={index}
                                className={this.state.tab === index ? "selected" : null}>{el}</li>)
                    }
                </ul>
                {
                    this.props.children.map((el, index) => {
                        if (this.state.tab === index) return el
                        else return
                    })
                }
            </React.Fragment>

        else return null
    }
}
Tabs.propTypes = {
    names: PropTypes.array.isRequired
}
