import React from "react"
import "./Tab.scss"
/**
 * Tab content component
 * @Children  required  - tab content
 */


export default class Tab extends React.Component{
    render(){
        return(
            <div className="tab-content">
                {this.props.children}
            </div>
        )
    }
}
